//
//  Device.swift
//  JJDevices
//
//  Created by Kevin O'Sullivan on 26/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import RealmSwift
import ObjectMapper
import AlamofireObjectMapper

class Device: Object, Mappable {
    dynamic var id = Int(NSDate().timeIntervalSince1970)
    dynamic var device = ""
    dynamic var os = ""
    dynamic var manufacturer = ""
    dynamic var lastCheckedOutDate = Date()
    dynamic var lastCheckedOutBy = ""
    dynamic var isCheckedOut = false
       
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        let transform = TransformOf<Date, String>(fromJSON: { (value: String?) -> Date? in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"
            let date = dateFormatter.date(from: value!)

            return date
            }, toJSON: { (value: Date?) -> String? in
                // transform value from Int? to String?
                if let value = value {
                    return value.toAPIString()
                }
                return nil
        })

        id <- map["id"]
        device <- map["device"]
        os <- map["os"]
        manufacturer <- map["manufacturer"]
        lastCheckedOutDate <- (map["lastCheckedOutDate"], transform)
        lastCheckedOutBy <- map["lastCheckedOutBy"]
        isCheckedOut <- map["isCheckedOut"]
        
    }
    
}
