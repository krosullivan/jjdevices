//
//  DeviceOptions.swift
//  JJDevices
//
//  Created by Kevin O' Sullivan on 29/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import UIKit

struct DeviceOptions {
    let manufacutures:  [String : [String]] = [
        "Apple": [
            "iPhone 5", "iPhone 6", "iPhone 7"
        ],
        "Google": [
            "Nexus 5", "Nexus 6", "Pixel"
        ],
        "HTC": [
            "HTC 10", "Desire"
        ]
    ]
    
    func getDevciesBy(manufacturer: String) -> [String] {
        if (manufacutures[manufacturer]!.count > 0) {
            return manufacutures[manufacturer]!
        } else {
            return []
        }
        
    }
}
