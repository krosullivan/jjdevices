//
//  MasterViewController.swift
//  JJDevices
//
//  Created by Kevin O' Sullivan on 22/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: CheckoutViewController? = nil
    var objects = [Device]()


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.leftBarButtonItem = self.editButtonItem

        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showCreateDeviceScreen))
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? CheckoutViewController
        }
        

        self.setupPulltoRefresh()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(handleSync), name: NSNotification.Name("SyncComplete"), object: nil)
        self.setData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("SyncComplete"), object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupPulltoRefresh() {
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.addTarget(self, action: #selector(handleRefresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl!)
    }

    func insertNewObject(_ sender: Any) {
        //objects.insert(NSDate(), at: 0)
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    func showCreateDeviceScreen() {
        self.performSegue(withIdentifier: "CreateDevice", sender: self)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
            
                let object = objects[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! CheckoutViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = objects[indexPath.row]
        cell.textLabel!.text = self.buildCellLabels(device: object).label
        cell.detailTextLabel!.text = self.buildCellLabels(device: object).detailLabel
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.deleteDevice(at: indexPath)
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }

    func handleRefresh() {
        SyncClient().sync()
    }

    func handleSync() {
        self.setData()
    }
    
    func setData() {
        let devices = Devices()
        objects = devices.get()
        self.tableView.reloadData()
        refreshControl?.endRefreshing()
    }
    
    func deleteDevice(at: IndexPath) {
        let devices = Devices()
        let device = objects[at.row]
        devices.delete(device: device)
        objects.remove(at: at.row)
        tableView.deleteRows(at: [at], with: .fade)
    }
    
    func buildCellLabels(device: Device) -> (label:String, detailLabel: String) {
        return (
            label: "\(device.device) - \(device.os)",
            detailLabel: device.isCheckedOut ? "Checked out by \(device.lastCheckedOutBy)" : "Available"
        )
    }
}

