//
//  CreateDeviceViewController.swift
//  JJDevices
//
//  Created by Kevin O' Sullivan on 26/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import UIKit

class CreateDeviceViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var deviceTextField: UITextField?
    @IBOutlet var osTextField: UITextField?
    @IBOutlet var manufacturerTextField: UITextField?
    
    func configureView() {
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(save))
        self.navigationItem.rightBarButtonItem = saveButton
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        self.navigationItem.leftBarButtonItem = cancelButton

        self.title = "Add a Device"
        deviceTextField?.delegate = self
        osTextField?.delegate = self
        manufacturerTextField?.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func save() {
        let devices = Devices()
        if let deviceText = deviceTextField?.text,
            let osText = osTextField?.text,
            let manufacturerText = manufacturerTextField?.text {
            
            if (
                deviceText.characters.count == 0 ||
                osText.characters.count == 0 ||
                manufacturerText.characters.count == 0
                ) {
                let alertController = UIAlertController(title: "Incomplete", message: "You missed some fields", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            } else {
                let device = devices.create(device: deviceText, os: osText, manufacturer: manufacturerText)
                let api = DevicesAPI()
                api.create(device: device, success: {_ in }, failure: {_ in})
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func cancel() {
        if let deviceText = deviceTextField?.text,
            let osText = osTextField?.text,
            let manufacturerText = manufacturerTextField?.text {
            
            if (
                deviceText.characters.count > 0 ||
                    osText.characters.count > 0 ||
                    manufacturerText.characters.count > 0
                ) {
                let alertController = UIAlertController(title: "Warning", message: "You will lose any information entered", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Continue", style: .default, handler:{ action in
                    self.dismiss(animated: true, completion: nil)
                }))
                alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        guard let text = textField.text else { return true }
        
        let newLength = text.utf16.count + string.utf16.count - range.length
        return newLength <= 50
    }
    
}
