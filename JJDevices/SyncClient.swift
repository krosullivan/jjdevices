//
//  SyncClient.swift
//  JJDevices
//
//  Created by Kevin O'Sullivan on 26/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import UIKit
import ReachabilitySwift

class SyncClient: NSObject {
    
    override init(){
        super.init()
        
    }
    
    func sync() {        
        
        self.getRemoteDevices(success: { remoteDevices in
            let devices = Devices()
            let localDevices = devices.get()
            let deltas = self.delta(localDevices: localDevices, remoteDevices: remoteDevices)
            self.updateDevices(devices: deltas.toSave)
            self.sendCreate(devices: deltas.toCreate)
            self.sendUpdate(devices: deltas.toUpdate)
        })
    }
    
    func getRemoteDevices(success:  @escaping ([Device]) -> ()){
        let devicesAPI = DevicesAPI()
        devicesAPI.get(
            success: { result in
                success(result)
            },
                failure: { error in
                print(error)
            }
        )
    }
    
    func getLocalDevices() -> [Device]{
        let devices = Devices()
        return devices.get()
    }
    
    func updateDevices(devices: [Device]) {
        
        let dev = Devices()
        for device in devices {
            _ = dev.update(device: device)
        }
        
        
        let notification = Notification(name: Notification.Name("SyncComplete"))
        NotificationCenter.default.post(notification)
    }
    
    func sendCreate(devices: [Device]) {
        let devicesApi = DevicesAPI()
        for device in devices {
            devicesApi.create(device: device, success: {_ in }, failure: {_ in })
        }
    }
    
    func sendUpdate(devices: [Device]) {
        let devicesApi = DevicesAPI()
        for device in devices {
            devicesApi.update(device: device, success: {_ in }, failure: {_ in })
        }
    }
    
    func delta(localDevices: [Device], remoteDevices: [Device]) -> (toSave: [Device], toCreate: [Device], toUpdate: [Device]) {
        var devicesToSave: [Device] = []
        var devicesToCreate: [Device] = []
        var devicesToUpdate: [Device] = []
        for remoteDevice in remoteDevices {
            let match = localDevices.filter({$0.id == remoteDevice.id}).first
            if let unwrappedMatch = match {
                if (remoteDevice.lastCheckedOutDate > unwrappedMatch.lastCheckedOutDate) {
                    devicesToSave.append(remoteDevice)
                } else {
                    devicesToUpdate.append(remoteDevice)
                }
            } else {
                devicesToSave.append(remoteDevice)
            }
        }
        
        for localDevice in localDevices {
            let match = remoteDevices.filter({$0.id == localDevice.id}).first
            if (match == nil) {
                devicesToCreate.append(localDevice)
            }
        }
        
        return (devicesToSave, devicesToCreate, devicesToUpdate)
    }
    
    private func startReachability() {
        let reachability = Reachability()!
        
        reachability.whenReachable = { reachability in
            DispatchQueue.main.async {
                if reachability.isReachableViaWiFi {
                    print("Reachable via WiFi")
                } else {
                    print("Reachable via Cellular")
                }
            }
        }
        
        reachability.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                print("Not reachable")
            }
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
        }
    }
}
