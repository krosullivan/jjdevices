//
//  CheckoutViewController.swift
//  JJDevices
//
//  Created by Kevin O' Sullivan on 22/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import UIKit

class CheckoutViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var deviceLabel: UILabel!
    @IBOutlet weak var osLabel: UILabel!
    @IBOutlet weak var manufacturerLabel: UILabel!
    @IBOutlet weak var checkedoutLabel: UILabel!
    @IBOutlet weak var checkoutBtn: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var selectDeviceLabel: UILabel!
    
    var nameTextField: UITextField?
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem,
            let stackView = self.stackView,
            let selectDevice = self.selectDeviceLabel {
            deviceLabel?.text = "Device: \(detail.device)"
            osLabel?.text = "OS: \(detail.os)"
            manufacturerLabel?.text = "Manufacturer: \(detail.manufacturer)"
            checkedoutLabel?.text = "Last Checked Out: \(detail.lastCheckedOutBy) on \(detail.lastCheckedOutDate.toDisplayString())"
            updateButtonLabel()
            stackView.isHidden = false
            selectDevice.isHidden = true
        }
    }
    
    @IBAction func checkoutTapped(sender: UIButton) {
        
        if let device = self.detailItem {
            if (device.isCheckedOut) {
                self.checkin(device: device)
            } else {
                self.showCheckoutAlert()
            }
        }
    }

    func updateButtonLabel(){
        if let device = self.detailItem,
            let btn = checkoutBtn,
            let checkedoutLabel = self.checkedoutLabel {
            if (device.isCheckedOut) {
                checkedoutLabel.text = "Last Checked Out: \(device.lastCheckedOutBy) on \(device.lastCheckedOutDate.toDisplayString())"
            } else {
                checkedoutLabel.text = ""
            }
            
            btn.setTitle(device.isCheckedOut ? "Check In" : "Check Out", for: UIControlState.normal)
        }
    }
    
    func addTextField(textField: UITextField!){
        nameTextField = textField
        nameTextField?.delegate = self
        
    }
    
    func nameEntered(alert: UIAlertAction!){
        if let device = self.detailItem, let text = nameTextField?.text {
            self.checkout(device: device, by: text)
        }
    }
    
    func checkin(device: Device) {
        Devices().checkin(device: device)
        updateButtonLabel()
    }
    
    func checkout(device: Device, by: String) {
        Devices().checkout(device: device, by: by)
        updateButtonLabel()
    }
    
    func showCheckoutAlert() {        
        let alertController = UIAlertController(title: "Check Out", message: "Please enter your name.", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addTextField(configurationHandler: addTextField)
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nameEntered))
        present(alertController, animated: true, completion: nil)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: Device? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        guard let text = textField.text else { return true }
        
        let newLength = text.utf16.count + string.utf16.count - range.length
        return newLength <= 50
    }

}

