//
//  Devices.swift
//  JJDevices
//
//  Created by Kevin O'Sullivan on 26/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import UIKit
import RealmSwift

class Devices: NSObject {
    
    func create(device: String, os: String, manufacturer: String) -> Device {
        let device = Device(value: ["device" : device, "os": os, "manufacturer": manufacturer]);
        let realm = try! Realm()
        try! realm.write {
            realm.add(device)
        }
        
        return device
    }
    
    func get() -> [Device] {
        let realm = try! Realm();
        let fetchedDevices = realm.objects(Device.self)
        return Array(fetchedDevices)
    }
    
    func getOne(id: Int) -> Device {
        let realm = try! Realm();
        let fetchedDevices = realm.objects(Device.self).filter("id = \(id)")
        return fetchedDevices[0]
    }
    
    func update(device: Device) -> Device {
        let realm = try! Realm();
        try! realm.write {
            realm.add(device, update: true)
        }
        
        return device
    }
    
    func checkin(device: Device) {
        let realm = try! Realm();
        try! realm.write {
            device.isCheckedOut = false
            realm.add(device, update: true)
            let api = DevicesAPI()
            api.update(device: device, success: {_ in }, failure: {_ in})
        }
    }
    
    func checkout(device: Device, by: String) {
        let realm = try! Realm();
        try! realm.write {
            device.isCheckedOut = true
            device.lastCheckedOutBy = by
            realm.add(device, update: true)
            let api = DevicesAPI()
            api.update(device: device, success: {_ in }, failure: {_ in})
        }
    }
    
    func delete(device: Device) {
        let realm = try! Realm()
        try! realm.write {
            let api = DevicesAPI()
            api.delete(id: device.id, success: {_ in }, failure: {_ in})
            realm.delete(device)
        }
    }
}
