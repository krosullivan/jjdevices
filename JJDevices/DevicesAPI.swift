//
//  DevicesAPI.swift
//  JJDevices
//
//  Created by Kevin O' Sullivan on 22/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import Foundation
import Alamofire

typealias SuccessHandler = (AnyObject)
typealias SuccessArrayHandler = ([Device])
typealias ErrorHandler = (NSError)

class DevicesAPI: NSObject {
    let basePath = "http://private-1cc0f-devicecheckout.apiary-mock.com/devices"
    
    func create(
        device: Device,
        success: @escaping (SuccessHandler) -> (),
        failure: @escaping (ErrorHandler) -> ()
        ) {
        
        let params: [String: Any] = [
            "device": device.device,
            "os": device.os,
            "manufacturer": device.manufacturer
        ]
        Alamofire.request(self.basePath,  method: .post, parameters: params, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.jsonCompletionHandler(response: response, success: success, failure: failure)
        }
    }
    
    func get(
        success: @escaping (SuccessArrayHandler) -> (),
        failure: @escaping (ErrorHandler) -> ()
        ) {
        Alamofire.request(self.basePath)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseArray { (response: DataResponse<[Device]>) in
                self.arrayCompletionHandler(response: response, success: success, failure: failure)
        }
    }
    
    func getOne(
        id:String,
        success: @escaping (SuccessHandler) -> (),
        failure: @escaping (ErrorHandler) -> ()
        ) {
        Alamofire.request("\(self.basePath)/\(id)").responseJSON { response in
            self.jsonCompletionHandler(response: response, success: success, failure: failure)
        }
    }
    
    func update(
        device: Device,
        success: @escaping (SuccessHandler) -> (),
        failure: @escaping (ErrorHandler) -> ()
        ) {
        let id = device.id
        var params: [String: Any]
        if (device.isCheckedOut) {
            params = [
                "lastCheckedOutDate": device.lastCheckedOutDate.toAPIString() ,
                "lastCheckedOutBy": device.lastCheckedOutBy,
                "isCheckedOut": device.isCheckedOut
            ]
        } else {
            params = [
                "isCheckedOut": device.isCheckedOut
            ]
        }
        
        Alamofire.request("\(self.basePath)/\(id)",  method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { response in
            success("Updated" as SuccessHandler)
        }
    }
    

    func delete(
        id:Int,
        success: @escaping (SuccessHandler) -> (),
        failure: @escaping (ErrorHandler) -> ()
        ) {
        Alamofire.request("\(self.basePath)/\(id)",  method: .delete).responseJSON { response in
            self.jsonCompletionHandler(response: response, success: success, failure: failure)
        }
    }

    private func arrayCompletionHandler(response: DataResponse<[Device]>, success: @escaping (SuccessArrayHandler) -> (), failure: @escaping (ErrorHandler) -> ()) {
        switch response.result {
        case .success:
            success(response.result.value! as SuccessArrayHandler);
        case .failure(let error):
            failure(error as ErrorHandler);
        }
    }
    
    private func jsonCompletionHandler(response: DataResponse<Any>, success: @escaping (SuccessHandler) -> (), failure: @escaping (ErrorHandler) -> ()) {
        switch response.result {
        case .success:
            success(response.result.value as SuccessHandler);
        case .failure(let error):
            failure(error as ErrorHandler);
        }
    }

}
