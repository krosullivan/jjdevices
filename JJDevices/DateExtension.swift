//
//  NSDateExtension.swift
//  JJDevices
//
//  Created by Kevin O' Sullivan on 28/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import UIKit

extension Date {
    
    func toAPIString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"
        return formatter.string(from: self)
    }
    
    func toDisplayString() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.short
        formatter.timeStyle = DateFormatter.Style.short
        return formatter.string(from: self)
    }
}
