# README #

The is the the iOS application used to check in devices from the J&J test devices storage. There is an trello board [here](https://trello.com/b/V3nCq4kz/ios-devices) where I have tracked my task progress and logged and outstanding bugs or features.

### How do I get set up? ###

* Clone the latest tagged version of this repo
* Install the dependencies with pod install
* There are unit tests which can be run from Xcode
* There are no UI tests although I did set up the project for them, so the base ones are there.
* I did not create a local mock for the API tests so an internet connection will be required. I would normally not do this but didn't have time to implement the mock/stub

### Problems Encountered ###

* The mock API misuses the POST verb. It uses POST for both creation and updates of resources. I would expect it to use POST for updates only and PUT for creation. I had to design the API logic around this.
* The example update API call only contains a subset of the resource fields. In this case I think POST is incorrect again and PATCH should be used.
* The use of incremental IDs is unusual (and maybe incorrect) in resources that are to be synced across mobile devices and presents some problems. For resources that are created on a device while offline, a complicated logic layer will need to be implemented across the device and server to try and figure out the correct ID during the REST creation.
* The response of the create call always result in the new ID being "5". This is going break the model layer as IDs are the primary key and have to be unique. For this reason I had to disregard the creation response id and let the device create its own. Again, I would not do this in a real project.

### Approach Taken ###

* As the project was under a very short time constraint I used Realm for the persistence layer.
* I chose this because of the minimal amount of boilerplate setup required, it is very quick to implement and very easy to write tests around
* To simplify REST calls and response object mapping I used the latest version of Alamofire, with the Object Mapper extension
* The object mapper works very well with Realm so this combination of technologies shaved off a considerable amount of time
* The model layer is very simple. There is just one model object called Device, with is a Mappable Realm object. 
* Responses from Alamofire map directly to this which works well for the most part. I had to implement a date transform, to convert the date string to Date object. I also create a DateExtension to handle date display and api string formats
* To handle syncing I created a SyncClient class. It uses a delta check on devices it has found from the server against devices it has stored locally. It saves the result on the delta function in a tuple with the form: (toSave: [Device], toCreate: [Device], toUpdate: [Device]). I then have function to preform the necessary actions on the values stored
* This Sync approach is not scalable or efficient, obviously limited to the API being just a mock service. I would prefer to implement a question and answer style approach. i.e. before the delta function to device tells the server what it has, the server responds with what it needs to do. As opposed to fetching everything all the time from the server.
* The sync engine posts a notification when it is done so that anyone listening can update as necessary.
* I tried to keep as much data or http logic away from the view controllers as possible. For this reason I created the Devices class to obfuscate the logic as much as possible. Its really very a simple facade pattern. This way the view controllers just have to worry about presenting the data and making the right calls
* For layouts I used stack views and layout constraints. This way the UI is consistent across devices of all sizes. I did not have to use size classes with this approach as the UI is so simple.
* To handle network conditions I implement [Reachability](https://github.com/ashleymills/Reachability.swift)

### TODOs ###

I only really had a couple of spare hours to spend on this project, so I didn't get to everything and rushed through some areas. There are a few things I would have liked to do, mostly enhancements not on the spec

* Picker views for device creation. I really don't like the idea of type things that should have consistent values. Picker views would be much better for things like selecting and OS or Device name. I had started a Struct to contain all available options for the device creation screen, and implemented a picker view as the input view for the text fields. I abandoned due to time constraints
* Better looking UI. It would be nice to get some styling in or more dynamic UX. I had to go with the layout as specified though
* iOS 8 support. Alamofire latest doesn't support iOS 8. An oversight on my part, which I didn't realise until late. The API architecture logic means that it would be easy to implement a new Network layer though