//
//  DevicesAPISwift.swift
//  JJDevices
//
//  Created by Kevin O' Sullivan on 22/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import XCTest
@testable import JJDevices

class DevicesAPITests: TestCaseBase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
   
    func testGetDevices() {
        let asyncExpectation = expectation(description: "HTTP Call")
        let devicesAPI = DevicesAPI();
        devicesAPI.get(
            success: { result in
                print(result)
                asyncExpectation.fulfill()
            },
            failure: { error in
                print(error)
            }
        )
        
        self.waitForExpectations(timeout: 5, handler: { error in
            XCTAssertNil(error, "Request Error")
        })
        
    }

    func testDeleteDevice() {
        let asyncExpectation = expectation(description: "HTTP Call")
        let devicesAPI = DevicesAPI();
        devicesAPI.delete(
            id: 1,
            success: { result in
                print(result);
                asyncExpectation.fulfill()
            },
            failure: { error in
                print(error);
            }
        )
        
        self.waitForExpectations(timeout: 5, handler: { error in
            XCTAssertNil(error, "Request Error")
        })
    }
    
    func testUpdateDevice() {
        let asyncExpectation = expectation(description: "HTTP Call")
        let devicesAPI = DevicesAPI();
        let updatePayload = Device(value: [
            "lastCheckedOutDate": NSDate(),
            "lastCheckedOutBy": "Richard Davies",
            "isCheckedOut": true
        ])
        
        devicesAPI.update(device: updatePayload,
                          success: { result in
                            print(result)
                            asyncExpectation.fulfill()
                          },
                          failure: { error in
                            print(error)
        })
        
        self.waitForExpectations(timeout: 5, handler: { error in
            XCTAssertNil(error, "Request Error")
        })
    }
    
    func testCreateDevice() {
        let asyncExpectation = expectation(description: "HTTP Call")
        let devicesAPI = DevicesAPI();
        let createPayload = Device(value: [
            "id": 5,
            "device": "iPhone 6 Plus",
            "os": "iOS 9.3",
            "manufacturer": "Apple",
            "isCheckedOut": false
        ])
        
        devicesAPI.create(device: createPayload,
                          success: { result in
                            print(result)
                            asyncExpectation.fulfill()
                          },
                          failure: { error in print(error) })
        
        self.waitForExpectations(timeout: 5, handler: { error in
            XCTAssertNil(error, "Request Error")
        })

    }

}
