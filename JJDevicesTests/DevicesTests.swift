//
//  DevicesAPISwift.swift
//  JJDevices
//
//  Created by Kevin O' Sullivan on 22/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import XCTest
import RealmSwift
@testable import JJDevices

class DevicesTests: TestCaseBase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let devices = Devices()
        let device = devices.create(device: "test device", os: "iOS 9", manufacturer: "Apple")
        XCTAssert(device.device == "test device", "Field not saved")
        XCTAssert(device.os == "iOS 9", "Field not saved")
        XCTAssert(device.manufacturer == "Apple", "Field not saved")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
   
    func testGet() {
        let devices = Devices()
        let fetchedDevices = devices.get();
        XCTAssert(fetchedDevices.count == 1, "Objects not found")
    }
    
    func testUpdate() {
        let devices = Devices()
        let device = Device(value: ["id": 0, "device" : "updated"]);
        _ = devices.update(device: device)
        let fetched = devices.getOne(id: 0)
        XCTAssert(fetched.device == "updated", "Object not found")
    }
}
