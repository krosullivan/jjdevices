//
//  SyncClient.swift
//  JJDevices
//
//  Created by Kevin O' Sullivan on 26/10/2016.
//  Copyright © 2016 Kevin O' Sullivan. All rights reserved.
//

import XCTest
@testable import JJDevices

class SyncClientTests: TestCaseBase {
        
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUpdateDevices() {
        let sc = SyncClient()
        let testDevice = Device(value: ["device" : "test", "os": "iOS", "manufacturer": "apple"]);
        let devices: [Device] = [testDevice]
        sc.updateDevices(devices: devices);
        
        let ds = Devices()
        let fetched = ds.getOne(id: testDevice.id)
        XCTAssert(fetched.device == "test", "Device not found")
        
    }
    
}
